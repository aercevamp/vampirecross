extends Node2D
var towers = 3
signal hunter_win

func _ready() -> void:
	pass 

func tower_lost():
	towers -= 1
	if towers < 1:
		emit_signal("hunter_win")

func _on_Tower_destroyed_tower() -> void:
	tower_lost()


func _on_Tower2_destroyed_tower() -> void:
	tower_lost()


func _on_Tower3_destroyed_tower() -> void:
	tower_lost()
