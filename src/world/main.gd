extends Node2D

#IMPORTANT: Sometimes nothing load, I think this is a load speed bug

onready var hunter = preload("res://src/hunter/Hunter.tscn").instance()
onready var lord = preload("res://src/lord/Lord.tscn").instance()
onready var level = preload("res://src/world/level_1.tscn").instance()
onready var tower = preload("res://src/lord/towers/Tower.tscn")
onready var lord_victory = preload("res://src/world/lordwin.tscn").instance()
var inverted_roles = null
#onready var minimap = preload("res://src/world/minimap.tscn").instance()
var pillar_id = -1

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	if inverted_roles == false:
		if get_tree().is_network_server():
			lord.set_network_master(get_tree().get_network_connected_peers()[0])
		else:
			# For the client, give control of player 2 to itself.
			lord.set_network_master(get_tree().get_network_unique_id())
	else:
		if get_tree().is_network_server():
			lord.set_network_master(get_tree().get_network_unique_id())
			hunter.set_network_master(get_tree().get_network_connected_peers()[0])
		else:
			# For the client, give control of player 2 to itself.
			lord.set_network_master(get_tree().get_network_connected_peers()[0])
			hunter.set_network_master(get_tree().get_network_unique_id())

#	var pillar_spots = level.get_used_cells_by_id(1)
#	for i in pillar_spots:
#		pillar_id += 1
#		var current_tower = tower.instance()
#		current_tower.id = pillar_id
#		current_tower.connect("kill_spell", lord, "on_kill_spell")
#		current_tower.set_global_position(i * 64) #Need to add 32,32 for it to be centered.
#		add_child(current_tower)
#		$Label.set_text(str(i) + " " + str(pillar_spots))

	hunter.position += Vector2(300,300)
	hunter.connect("lord_won", self, "on_who_won")
	level.connect("hunter_win", self, "on_who_lost")
	add_child(level)
	add_child(hunter)
	add_child(lord)
#	add_child(minimap) bugged feature
#	minimap.minimap.target = hunter

func on_who_won():
	$Camera2D.current = true

func on_who_lost():
	$Camera2D2.current = true


func _on_AudioStreamPlayer_finished() -> void:
	$"%AudioStreamPlayer2".play()
