extends Control
var SERVER_PORT: int = 46318
var SERVER_IP: String = ""
var MAX_PLAYERS: int = 1
var peer = null
var inverted_roles = false



func _ready() -> void:
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")



func _on_HostButton_pressed() -> void:
	SERVER_PORT = int($"%PortNumber".get_text())

	peer = NetworkedMultiplayerENet.new()
	var err = peer.create_server(SERVER_PORT, 1)
	if err != OK:
		# Is another server running?
		$"%infoLabel".set_text("Can't host, address in use.")
		return

	get_tree().set_network_peer(peer)
	$"%infoLabel".set_text("hosting rn")



func _on_JoinButton_pressed() -> void:
	SERVER_IP = $"%IPAdress".get_text()
	SERVER_PORT = int($"%PortNumber".get_text())
	print(str(SERVER_IP) + " " + str(SERVER_PORT))
	if not SERVER_IP.is_valid_ip_address():
		$"%infoLabel".set_text("IP BAD.")
		return

	peer = NetworkedMultiplayerENet.new()
	peer.create_client(SERVER_IP, SERVER_PORT)
	get_tree().set_network_peer(peer)

	$"%infoLabel".set_text("joinin rn")



func _player_connected(_id):
	$"%infoLabel".set_text("I have connected to your computer and can now commit crimes")
	var main = load("res://src/world/main.tscn").instance()
	main.inverted_roles = inverted_roles
	get_tree().get_root().add_child(main)
	$AudioStreamPlayer.stop()
	hide()

func _player_disconnected(_id):
	if get_tree().is_network_server():
		$"%infoLabel".set_text("Client disconnected")
	else:
		$"%infoLabel".set_text("Server disconnected")

func _connected_fail():
	$"%infoLabel".set_text("Can't connect")

func _server_disconnected():
	$"%infoLabel".set_text("Server disconnected")



func _on_connection_success():
	$"%infoLabel".set_text("yay connected")


func _on_CheckButton_toggled(button_pressed: bool) -> void:
	if button_pressed == true:
		inverted_roles = true
		$"%AnimationPlayer".play("invert")
	else:
		inverted_roles = false
		$"%AnimationPlayer".play("normal")



func _on_Button_pressed() -> void:
	SERVER_PORT = int($"%PortNumber".get_text())
	var upnp = UPNP.new()
	upnp.discover(2000, 2, "InternetGatewayDevice")
	upnp.add_port_mapping(SERVER_PORT)


func _on_Button2_pressed() -> void:
	$Button.visible = true
