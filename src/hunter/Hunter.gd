extends KinematicBody2D
var drag_factor:= 0.2
var velocity:= Vector2.ZERO
export var max_speed:= 600
onready var slash = preload("res://src/hunter/AtkSlash.tscn")
onready var lordwin = preload("res://src/world/lordwin.tscn")
onready var health = 200
var killcount = 0
signal lord_won

# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	set_meta("Type", "Hunter")
	if is_network_master():
		$"%Camera2D".make_current()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if is_network_master():
		var direction:= Vector2.ZERO
		direction.x = Input.get_axis("move_left", "move_right")
		direction.y = Input.get_axis("move_up", "move_down")
		direction = direction.normalized()
		
		var desired_velocity:= max_speed * direction
		var steering_vector:= desired_velocity - velocity
		velocity += steering_vector * drag_factor
		rpc("flip_sprite", velocity)
		
		move_and_slide(velocity)
		rpc_unreliable("set_pos", position)
		Autostats.hunter_position = position
#		if Input.is_action_just_pressed("space"):
#			rpc("slash")




remotesync func flip_sprite(velocity):
	if velocity.x < 0:
		$AnimatedSprite.flip_h = true
	elif velocity.x > 0:
		$AnimatedSprite.flip_h = false
	if abs(velocity.x) < 0.3 and abs(velocity.y) < 0.3:
		$AnimationPlayer2.play("idle")
	else: $AnimationPlayer2.play("run")


remotesync func slash():
	var attack = slash.instance()
	attack.scale = attack.scale * 1.3 #make scale grow with kill count
	#like connect death signal with some singleton or something
	add_child(attack)


puppet func set_pos(pos):
	position = pos #maybe only called this on fixed process?
	Autostats.hunter_position = position


func _on_HunterHitbox_body_entered(body: Node) -> void:
	if body.get_meta("Type") == "Mob" or body.get_meta("Type") == "Obstacle":
		health -= body.contact_damage
		$"%HealthDisplay".update_healthbar(health)
		$"%AnimationPlayer".play("Hitbox_blink")
		if health <= 0:
			die_hunter()
	elif body.get_meta("Type") == "Enemy_Projectile":
		health -= body.contact_damage
		$"%HealthDisplay".update_healthbar(health)
		$"%AnimationPlayer".play("Hitbox_blink")
		body.die_monster()
		if health <= 0:
			die_hunter()


func die_hunter():
	visible = false
	$HunterHitbox.monitoring = false
	emit_signal("lord_won")


func _on_SlashTimer_timeout() -> void:
	if is_network_master():
		rpc("slash")
