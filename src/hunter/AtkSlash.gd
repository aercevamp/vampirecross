extends Area2D
var already_hit = []
export var damage = 5

# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	$AnimationPlayer.play("SlashAnim")
	if is_network_master():
		var mouse = get_global_mouse_position()
		look_at(mouse)
		rotation -= 30
		rpc("rotate_to_mouse", rotation)

puppetsync func rotate_to_mouse(puppet_rotate):
	rotation = puppet_rotate
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass


func _on_Area2D_body_entered(body: Node) -> void: #maybe only if is_network_master
	if body.get_meta("Type") == "Mob" and already_hit.has(body) == false:
		already_hit.append(body) #making sure I can't use one hitbox for multiple hits
		body.take_damage(damage) #there's some desync potential in this being done in the lord's side too
		#add knockback here

func _on_AnimationPlayer_animation_finished(_anim_name: String) -> void:
	queue_free()
