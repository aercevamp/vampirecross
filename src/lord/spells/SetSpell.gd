extends Control
var nmbrspell = 0
#preload every spell?


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass
	if is_network_master():
		visible = true
	#setup_spells(["SpellBase","SpellBase","SpellBase"]) #We should get this from Lord on chara select


func setup_spells(spells): #We make the UI and instance the spells
	nmbrspell = 0 #this is terrible
	$"%Panel".set_size(Vector2(spells.size() * 100, 560)) #or whatever spell.x + margin is
	for i in spells:
		nmbrspell += 1 #seriously there's a better way I'm sure
		var temp_spell = load("res://src/lord/spells/" +i +".tscn").instance() #Is this alright?
		#if you were cool this would be preloaded base_node.change_attribute(i)
		temp_spell.rect_position.y = 560 - 32 #32 is half the placeholder, change later
		temp_spell.rect_position.x = 80 * nmbrspell - 32
		temp_spell.set_name("Spell_"+str(nmbrspell-1))
		add_child(temp_spell)
	for _i in get_children():
		print(_i)

