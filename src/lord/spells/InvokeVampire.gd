extends TextureButton
onready var timer = $Timer
onready var sweep = $Sweep
export var cooldown = 1.0
# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	timer.wait_time = cooldown
	sweep.value = 0
	sweep.texture_progress = texture_normal

func _process(delta):
	if timer.time_left > 0.0: #tutorial used set_process(false) etc
		sweep.value = int((timer.time_left / cooldown) * 100)


func activate_spell():
	if timer.time_left == 0.0:
		timer.start()
		var trap = "vampire"
		return trap
	else:
		return "pass"


func _on_Timer_timeout() -> void:
	sweep.value = 0
