extends KinematicBody2D
var found = false
var activated = false
var target = null
export var health = 30
export var speed = 2500
export var contact_damage = 50
var current_direction = null
# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	set_meta("Type", "Mob")
	$AnimationPlayer.play("Idle")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if found:
		if activated:
			move_and_slide(current_direction * speed)


func get_direction():
	var direction = (target.position - position).normalized()
	rotation = direction.angle()
	current_direction = direction


func _on_Vision_body_entered(body: Node) -> void:
	if body.get_meta("Type") == "Hunter": #works for any hunter just in case
		target = body
		found = true
		#maybe flash?
		$Timer.start()

#ONCE LIFE = 0 REMOTESYNC die_monster():

func take_damage(damage):
	health -= damage
	if health <= 0:
		rpc("die_monster")

#puppetsync take_damage_puppet() animation, no health stuff?
#let lord decide when stuff is dead and rpc

remotesync func die_monster():
	queue_free()


func _on_Timer_timeout() -> void:
	activated = true
	get_direction()
	$AnimationPlayer.play("Rush")
	$Timer2.start()


func _on_Timer2_timeout() -> void:
	activated = false
	rotation = 0
	$AnimationPlayer.play("Idle")
	$Timer.start()


func _on_Timer3_timeout() -> void:
	$Timer.stop()
	$Timer2.stop()
	$AnimationPlayer.play("Fade")


	


func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == "Fade":
		rpc("die_monster")
