extends KinematicBody2D
export var contact_damage = 25
# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	set_meta("Type", "Enemy_Projectile")



func _process(delta: float) -> void:
	position += Vector2(1, 0).rotated(rotation) * 300 * delta


func _on_Timer_timeout() -> void:
	$"%AnimationPlayer".play("fade_out")


func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	queue_free()

remotesync func die_monster():
	queue_free()
