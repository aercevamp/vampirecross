extends KinematicBody2D
var activated = false
var target = null
export var health = 5
export var speed = 200
export var contact_damage = 30
# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	set_meta("Type", "Mob")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if activated:
		var direction = (target.position - position).normalized()
		move_and_slide(direction * speed)
		if direction.x > 0:
			$Sprite.flip_h = true
		else:
			$Sprite.flip_h = false




func _on_Vision_body_entered(body: Node) -> void:
	if body.get_meta("Type") == "Hunter": #works for any hunter just in case
		target = body
		activated = true

#ONCE LIFE = 0 REMOTESYNC die_monster():

func take_damage(damage):
	health -= damage
	if health <= 0:
		rpc("die_monster")

#puppetsync take_damage_puppet() animation, no health stuff?
#let lord decide when stuff is dead and rpc

remotesync func die_monster():
	queue_free()


func _on_AFKTimer_timeout() -> void:
	if activated == false:
		rpc("die_monster")


func _on_DeathTimer_timeout() -> void:
	rpc("die_monster")
