extends KinematicBody2D
var found = false
var activated = false
var target = null
export var health = 10
export var speed = 200
export var contact_damage = 50
onready var projectile = preload("res://src/lord/creatures/Projectile.tscn")
# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	set_meta("Type", "Mob")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if found:
		if activated:
			var direction = (target.position - position).normalized()
			move_and_slide(direction * speed)
			if direction.x < 0:
				$Sprite.flip_h = true
			else:
				$Sprite.flip_h = false




func _on_Vision_body_entered(body: Node) -> void:
	if body.get_meta("Type") == "Hunter": #works for any hunter just in case
		target = body
		found = true
		activated = true
		$Timer.start()

#ONCE LIFE = 0 REMOTESYNC die_monster():

func take_damage(damage):
	health -= damage
	$AnimationPlayer.play("Blink")
	if health <= 0:
		rpc("die_monster")

#puppetsync take_damage_puppet() animation, no health stuff?
#let lord decide when stuff is dead and rpc

remotesync func die_monster():
	queue_free()


func _on_Timer_timeout() -> void:
	activated = false
	$Timer2.start()
	var temp_proj = projectile.instance()
	temp_proj.rotation = (target.position - global_position).angle()
#	temp_proj.position = position
	add_child(temp_proj)


func _on_Timer2_timeout() -> void:
	activated = true
	$Timer.start()


func _on_AFKTimer_timeout() -> void:
	if found == false:
		rpc("die_monster")


func _on_DeathTimer_timeout() -> void:
	rpc("die_monster")
