extends KinematicBody2D
export var contact_damage = 30



func _ready() -> void:
	#rotation = Autostats.hunter_position.angle()
	look_at(Autostats.hunter_position)
	set_meta("Type", "Enemy_Projectile")
	#find hunter rotation somehow
	$AnimationPlayer.play("Shoot")

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	queue_free()
