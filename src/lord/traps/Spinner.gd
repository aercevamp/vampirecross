extends KinematicBody2D
export var contact_damage = 15
export var health = 20
# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	set_meta("Type", "Obstacle")
	$AnimationPlayer.play("Nothing")


func _on_Timer_timeout() -> void:
	queue_free()

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == "Nothing":
		$Timer.start()
		$AnimationPlayer.play("Spin")

func take_damage(damage):  #whoops not implemented
	health -= damage
	$AnimationPlayer2.play("Blink")
	if health <= 0:
		rpc("die_monster")

remotesync func die_monster():
	queue_free()

