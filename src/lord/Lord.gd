extends Node2D
var cursorPosition = Vector2.ZERO
var selectedSpell =  0
var valid = true
export var speed = 60
var spellList = ["SpellBase","SummonLaser","InvokeBat","SummonSpinner", "InvokeVampire"]
onready var minimap = $"%HunterSpycam"
onready var skeleton = preload("res://src/lord/creatures/Skeleton.tscn") #Should be as needed
onready var spinner = preload("res://src/lord/traps/Spinner.tscn") 
onready var bat = preload("res://src/lord/creatures/Bat.tscn")
onready var laser = preload("res://src/lord/traps/Laser.tscn")
onready var vampire = preload("res://src/lord/creatures/Vampire.tscn")
onready var mob_list = {"skeleton":skeleton, "spinner":spinner, "bat":bat, "laser":laser, "vampire":vampire}
export var wheel_sensitivity = 0.1
var spellListDup = spellList.duplicate()
#Please turn "CanvasLayer/SetSpell/Spell_" & "CanvasLayer/SetSpell/Spell_" into % stuff

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	if is_network_master():
		$"%SetSpell".setup_spells(spellList)
	#could do in spells "for i in spelllist.length: instance spell, give it name & attributes"
	#do we hide SetSpell if not network master?
		var InitSpellPlayer = get_node("CanvasLayer/SetSpell/Spell_" + str(selectedSpell) + "/AnimationPlayer")
		InitSpellPlayer.play("spellSelected")
		$"%MainCam".make_current()
	
	var spawn_list = get_parent().level.get_used_cells_by_id(1)
	for i in spawn_list:
		pass #connect the instance's when_dead signal, give it an id and tie it to spells etc



func _process(delta: float) -> void:
	if is_network_master():
		var direction:= Vector2.ZERO
		direction.x = Input.get_axis("move_left", "move_right")
		direction.y = Input.get_axis("move_up", "move_down")
		direction = direction.normalized()
		
		$"%MainCam".position += direction * (speed * ($"%MainCam".zoom.x *0.2)) #Better to make the cam follow then smooth
		#also find a way to adapt it to camera zoom
		if Input.is_action_just_pressed("cycle_left"):
			select_spell(-1)
		elif Input.is_action_just_pressed("cycle_right"):
			select_spell(1)

		if Input.is_action_just_released("cycle_down_mouse"):
			if $"%MainCam".zoom.x < 4.0: $"%MainCam".zoom += Vector2(wheel_sensitivity, wheel_sensitivity)
		elif Input.is_action_just_released("cycle_up_mouse"):
			if $"%MainCam".zoom.x > 1.0: $"%MainCam".zoom -= Vector2(wheel_sensitivity, wheel_sensitivity)
			#This should be in its own .gd shouldn't it

		if Input.is_action_just_pressed("left_click"):
			if valid == true:
				var spellCode = get_node("CanvasLayer/SetSpell/Spell_" + str(selectedSpell))
				var result = spellCode.activate_spell()
				if result != "pass":
					var mob = mob_list[result].instance() #This needs a dictionary
					mob.position = $Cursor.position
					rpc("add_puppet_mob", result, mob.position)
					add_child(mob) #I could turn this into one remotesync func
			else: $"%AnimationPlayer".play("Fade")

puppet func add_puppet_mob(result, pos): #spawn mob reliably and in the correct place
	var mob = mob_list[result].instance()
	mob.position = pos
	add_child(mob)

func select_spell(number):
	var spellPlayer = get_node("CanvasLayer/SetSpell/Spell_" + str(selectedSpell) + "/AnimationPlayer")
	spellPlayer.play("RESET") #Reset previous spell
	
	var tempSelect = selectedSpell
	tempSelect += number
	if tempSelect < 0:
		tempSelect = spellList.size() - 1
	selectedSpell = tempSelect % spellList.size() #overly complicated spell selection
	$"%COOLDEBUG".set_text(str(selectedSpell)) 
	
	spellPlayer = get_node("CanvasLayer/SetSpell/Spell_" + str(selectedSpell) + "/AnimationPlayer")
	spellPlayer.play("spellSelected")


func is_space_valid(mousePosition):
	pass #will have to move chars if they spawn near walls while too big maybe?

func on_kill_spell(deadspell):
	if is_network_master():
		var fun = 0
		var old = spellList.duplicate()
		for i in spellList:
			$"%SetSpell".remove_child($"%SetSpell".get_node("Spell_" + str(fun)))
			fun += 1
		spellList.erase(spellListDup[deadspell]) #tried remove but there was an issue with
		#removing index 2 when the array was only 0,1
		$CanvasLayer/COOLDEBUG.set_text("btw spell list was " + str(old) + " is now" + str(spellList) + "and it's because " + str(deadspell))
		$"%SetSpell".setup_spells(spellList)
		selectedSpell =  0
	


func _on_Cursor_area_entered(area: Area2D) -> void:
	valid = false


func _on_Cursor_area_exited(area: Area2D) -> void:
	valid = true
