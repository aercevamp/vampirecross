extends Area2D

# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if is_network_master():
		position = get_global_mouse_position()
		rpc_unreliable("set_pos", position)
	

puppet func set_pos(pos):
	position = pos #this lags behind a bit somehow
