extends KinematicBody2D
export var health = 10
var id = 0
signal destroyed_tower

func _ready() -> void:
	set_meta("Type", "Mob") #because can take damage

func take_damage(damage):
	health -= damage
	if health <= 0:
		die_monster() #don't rpc here or it'll do it twice!

func die_monster():
	emit_signal("destroyed_tower")
	queue_free()
